# zf3-scrapping

## Getting Started

Sebagai pembelajaran untuk meengetahui proses scrapping pada zend framework 3 dengan <b>Zend_Http_Client</b>

Running local :
```bash
php -S 0.0.0.0:8080 -t public public/index.php
```

Kemudian ambil library zend.
```bash
composer install
```

Ambil data pada inspect element.
![inspect](https://gitlab.com/maulana20/zf3-scrapping/-/raw/master/screen/inspect.png)

Hasil scrap pencarian jadwal.
![scrapper](https://gitlab.com/maulana20/zf3-scrapping/-/raw/master/screen/scrapper.PNG)
