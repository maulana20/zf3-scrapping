var data = [{code: 'CGK', name: 'Cengkareng'},{code: 'BTH', name: 'Batam'},{code: 'UPG', name: 'Ujung Pandang'},{code: 'PLM', name: 'Palembang'},{code: 'BDO', name: 'Bandung'},{code: 'SUB', name: 'Surabaya'},{code: 'DPS', name: 'Denpasar'}];
var search_loading = false;

for (i=0; i < data.length; i++) {
	var airport = data[i].name + ' ' + data[i].code;
	
	// FROM
	var li = document.createElement('li');
	li.setAttribute('onclick', "select('from', '" + airport + "')");
	li.setAttribute('value', data[i].code);
	li.appendChild(document.createTextNode(airport));
	
	document.getElementById("dropdown-from").appendChild(li);
	
	var li = document.createElement('li');
	li.setAttribute('onclick', "select('to', '" + airport + "')");
	li.setAttribute('value', data[i].code);
	li.appendChild(document.createTextNode(airport));
	
	document.getElementById("dropdown-to").appendChild(li);
}

function filter(type)
{
	if (type == 'from') {
		var input = document.getElementById('from_code');
		var filter = input.value.toUpperCase();
		
		var div = document.getElementById('dropdown-from');
		var tag = div.getElementsByTagName('li');
	} else {
		var input = document.getElementById('to_code');
		var filter = input.value.toUpperCase();
		
		var div = document.getElementById('dropdown-to');
		var tag = div.getElementsByTagName('li');
	}
	
	for (i=0; i < tag.length; i++) {
		value = tag[i].getAttribute('value');
		if (filter.length > 2 && value.indexOf(filter) > -1) {
			tag[i].style.display = "";
		} else {
			tag[i].style.display = "none";
		}
	}
}

function select(type, code)
{
	var form = document.form;
	
	if (type == 'from') {
		form['from_code'].value = code;
		document.getElementById('dropdown-from').style.display = 'none';
	} else {
		form['to_code'].value = code;
		document.getElementById('dropdown-to').style.display = 'none';
	}
}

function search(url)
{
	var form = document.form;
	var params = 'from_code=' + form['from_code'].value.split(' ')[form['from_code'].value.split(' ').length - 1]
		+ '&to_code=' +form['to_code'].value.split(' ')[form['to_code'].value.split(' ').length - 1]
		+ '&date=' + form['date'].value
	;
	if (search_loading) {
		alert('Please wait until this screen is completely loaded !');
		return false;
	}
	search_loading = true;
	
	document.getElementById('searchload').style.display = 'block';
	document.getElementById('searchload').innerHTML = '<img src="/img/ajax-loader.gif" />';
	
	var ajax = new Ajax.Updater(
		{success: 'searchexpand'},
		url,
		{method: 'POST', parameters: params, onComplete: searchUpdate});
}

function searchUpdate(request)
{
	document.getElementById('searchload').innerHTML = '';
	document.getElementById('searchload').style.display = 'none';
	
	search_loading = false;
}
