<?php
namespace Application\Model;

use Zend\Http\Client;
use Exception;

abstract class Scrapper
{
	protected $url;
	protected $client;
	
	abstract protected function loginClient($username, $password);
	abstract protected function logoutClient();
	abstract protected function isSessionTimeout();
	abstract protected function search($date, $ori, $des, $adult, $child, $infant);
	
	protected function logResponse($file, $response)
	{
		$f = fopen($file, 'w');
		fwrite($f, $response);
		fclose($f);
	}

	public function start()
	{
		$this->url = 'https://book.citilink.co.id';
		
		if ($this->isSessionTimeout()) {
			$this->createClient();
			$this->loginClient($username, $password);
		}
	}
	
	protected function createClient()
	{
		$this->client = new Client($this->url);
		
		$this->client->setOptions([
			'timeout' => 60,
			'ssltransport' => 'sslv3',
			'keepalive' => true,
			'adapter'  => 'Zend\Http\Client\Adapter\Curl',
			'persistent' => true,
			'curloptions' => [ CURLOPT_SSL_VERIFYPEER => false, ],
		]);
		$this->client->setHeaders([ "Connection" => "keep-alive" ]);
	}
	
	public function stop()
	{
		$this->logoutClient();
	}
}

class Citilink extends Scrapper
{
	private function _getValueField($res)
	{
		$result = array();
		$body = $res;
		$body = str_replace("'", '"', $body);
		while (true) {
			$taginput = stristr($body, '<input');
			if ($taginput) {
				$taginput = substr($taginput, 0, strpos($taginput,">"));
				$tag = $taginput;
				$test = stristr($taginput, 'name="');
				if (empty($test)) {
					$taginput = stristr($taginput, 'id="');
				} else {
					$taginput = $test;
				}
				$taginput = stristr($taginput, '"');
				$matches = substr($taginput,1,@strpos($taginput,'"',1)-1);
				$name = $matches;
				
				$taginput = $tag;
				$taginput = stristr($taginput, 'value="');
				if (!empty($taginput)) {
					$taginput = stristr($taginput, '"');
					$matches = substr($taginput,1,strpos($taginput,'"',1)-1);
					
					$value = $matches;
				} else {
					$value = NULL;
				}
				if (@is_array($result[$name])) {
					$result[$name][] = $value;
				} else if (isset($result[$name])) {
					$temp = $result[$name];
					$result[$name] = array();
					$result[$name][] = $temp;
					$result[$name][] = $value;
				} else {
					$result[$name] = $value;
				}
				
				$body = stristr($body, '<input');
				$body = substr($body, 6);
			} else {
				break;
			}
		}
		
		return $result;
	}
	
	private function _matrix($res)
	{
		$data = $this->_getValueField($res);
		
		$markets = [];
		$markets = $data['ControlGroupScheduleSelectView$AvailabilityInputScheduleSelectView$market1'];
		
		foreach ($markets as $market) {
			$_market = [];
			$_market = explode('|', $market);
			
			$journey = null;
			$journey = $_market[1];
			
			$journey_data = [];
			$journey_data = explode('^', $journey);
			
			$is_transit = count($journey_data) > 1;
			
			$flight_data = $route_data = $time_depart_data = $time_arrive_data = $str_time_data = [];
			
			foreach ($journey_data as $k => $data) {
				$schedule_data = [];
				$schedule_data = explode('~', $data);
				
				$flight_data[$k] = $schedule_data[0] . ' ' . trim($schedule_data[1]);
				$route_data[$k] = $schedule_data[4] . '-' . $schedule_data[6];
				$time_depart_data[$k] = strtotime($schedule_data[5]);
				$time_arrive_data[$k] = strtotime($schedule_data[7]);
				$str_time_data[$k] = date('H:i', $time_depart_data[$k]) . '##' . date('H:i', $time_arrive_data[$k]);
			}
			
			$flight_code = implode('##', $flight_data);
			if (!$is_transit) $str_time_data[0] = date('H:i', $time_depart_data[0]) . ' ' . date('H:i', $time_arrive_data[0]);
			
			if ($flight_temp != $flight_code) {
				$flight_temp = $flight_code;
				
				$i++;
				
				$result[$i]['id'] = $i;
				$result[$i]['flight'] = $flight_code;
				$result[$i]['route'] = implode('##', $route_data);
				$result[$i]['time_depart'] = (string) implode('##', $time_depart_data);
				$result[$i]['time_arrive'] = (string) implode('##', $time_arrive_data);
				$result[$i]['str_time'] = implode(' ', $str_time_data);
				$result[$i]['str_date'] = date("d-M-Y", $time_depart_data[0]);
				$result[$i]['longdate'] = $time_depart_data[0];
				$result[$i]['weekday'] = strtoupper(date("D", $time_depart_data[0]));
				
				$j = -1;
			}
		}
		
		return $result;
	}
	public function loginClient($username, $password)
	{
		$client = &$this->client;
		$host = $this->url;
		
		$client->resetParameters();
		
		$client->setUri($host);
		$client->setHeaders(["Connection" => "keep-alive"]);
		
		try {
			$response = $client->send();
			$result = $response->getBody();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
		
		$this->logResponse('log/CitilinkIndex.html', $result);
	}
	
	public function logoutClient()
	{
	}
	
	public function isSessionTimeout()
	{
		return true;
	}
	
	public function search($date, $ori='CGK', $des='BTH', $adult=1, $child=0, $infant=0)
	{
		$client = &$this->client;
		$host = $this->url;
		
		if (empty($date)) throw new Exception('Date not found !');
		if (empty($ori)) throw new Exception('Origin not found !');
		if (empty($des)) throw new Exception('Destination not found !');
		
		$client->resetParameters();
		$data = [];
		$data['Page'] = 'Select';
		$data['RadioButtonMarketStructure'] = 'OneWay';
		$data['TextBoxMarketOrigin1'] = $ori;
		$data['TextBoxMarketDestination1'] = $des;
		$data['DropDownListMarketMonth1'] = date('Y-m', $date);
		$data['DropDownListMarketDay1'] = date('d', $date);
		$data['DropDownListMarketMonth2'] = ''; 
		$data['DropDownListMarketDay2'] = ''; 
		$data['DropDownListCurrency'] = 'IDR';
		$data['OrganizationCode'] = 'QG';
		$data['DdlCabinDynamic'] = 'NR';
		$data['CheckBoxFareTypeValue'] = 'RegularFlight';
		$data['DropDownListPassengerType_ADT'] = $adult;
		$data['DropDownListPassengerType_CHD'] = $child;
		$data['DropDownListPassengerType_INFANT'] = $infant;
		$data['DropDownListPassengerType_SRC'] = 0;
		$data['culture'] = 'id-ID';
			
		$client->setUri($host . '/Search.aspx');
		$client->setMethod('GET');
		$client->setParameterGet($data);
		
		try {
			$response = $client->send();
			$result = $response->getBody();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
		
		$this->logResponse('log/CitilinkSearch.html', $result);
		
		return $matrix = $this->_matrix($result);
	}
}
