<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

class Module
{
	const VERSION = '3.1.4dev';
	
	public function getConfig()
	{
		return [
			'router' => [
				'routes' => [
					'home' => [
						'type'    => Literal::class,
						'options' => [
							'route'    => '/',
							'defaults' => [
								'controller' => Controller\IndexController::class,
								'action'     => 'index',
							],
						],
					],
					'application' => [
						'type'    => Segment::class,
						'options' => [
							'route'    => '/application[/:action]',
							'defaults' => [
								'controller' => Controller\IndexController::class,
								'action'     => 'index',
							],
						],
					],
					'scrapper' => [
						'type'    => Segment::class,
						'options' => [
							'route'    => '/scrapper[/:action]',
							'defaults' => [
								'controller' => Controller\ScrapperController::class,
								'action'     => 'index',
							],
						],
					],
				],
			],
			'controllers' => [
				'factories' => [
					Controller\IndexController::class => InvokableFactory::class,
					Controller\ScrapperController::class => InvokableFactory::class,
				],
			],
			'view_manager' => [
				'display_not_found_reason' => true,
				'display_exceptions'       => true,
				'doctype'                  => 'HTML5',
				'not_found_template'       => 'error/404',
				'exception_template'       => 'error/index',
				'template_map' => [
					'layout/layout'           => dirname(__DIR__) . DIRECTORY_SEPARATOR . __NAMESPACE__ . '/view/layout/layout.phtml',
					'application/index/index' => dirname(__DIR__) . DIRECTORY_SEPARATOR . __NAMESPACE__ . '/view/application/index/index.phtml',
					'error/404'               => dirname(__DIR__) . DIRECTORY_SEPARATOR . __NAMESPACE__ . '/view/error/404.phtml',
					'error/index'             => dirname(__DIR__) . DIRECTORY_SEPARATOR . __NAMESPACE__ . '/view/error/index.phtml',
				],
				'template_path_stack' => [
					dirname(__DIR__) . DIRECTORY_SEPARATOR . __NAMESPACE__ . '/view',
				],
			],
		];
	}
}
