<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Model\Citilink;

class ScrapperController extends AbstractActionController
{
	public function indexAction()
	{
		$view = new ViewModel();
		return $view->setTemplate('application/scrapper/index');
	}
	
	public function searchAction()
	{
		$citilink = new Citilink();
		
		$request = $this->getRequest();
		
		$date = $request->getPost('date');
		$from_code = $request->getPost('from_code');
		$to_code = $request->getPost('to_code');
		$adult = $request->getPost('adult');
		$child = $request->getPost('child');
		$infant = $request->getPost('infant');
		
		$citilink->start();
		$list = $citilink->search(strtotime($date), $from_code, $to_code, 1, 0, 0);
		
		$view = new JsonModel(['from_code' => $from_code, 'to_code' => $to_code, 'list' => $list]);
		return $view->setTerminal(true);
	}
}
